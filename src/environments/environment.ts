// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDF9Q-0BzfDL3wItgqtdtbLN-OqpKSkZbg",
    authDomain: "fyc-project-d6e95.firebaseapp.com",
    databaseURL: "https://fyc-project-d6e95.firebaseio.com",
    projectId: "fyc-project-d6e95",
    storageBucket: "fyc-project-d6e95.appspot.com",
    messagingSenderId: "626477648027",
    appId: "1:626477648027:web:71cd17a5ea681cda9a1e3e",
    measurementId: "G-CPESV7J2RZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
